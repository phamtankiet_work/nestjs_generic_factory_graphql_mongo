###############################################################################
#  -------- package json preprocessor --------
# To prevent cache invalidation from changes in fields other than dependencies (version number)
###############################################################################
FROM alpine:latest AS preprocessor
RUN apk add --update --no-cache curl jq

COPY package*.json /tmp/
RUN jq '{ dependencies, devDependencies }' < /tmp/package.json > /tmp/deps.json
# RUN jq '{ name, lockfileVersion, requires, dependencies }' < /tmp/package-lock.json > /tmp/deps-lock.json


###############################################################################
#  -------- base image with production package only --------
# To cache all production dependencies in a single layer
###############################################################################
FROM node:18-alpine AS base_image

WORKDIR /usr/src/app

RUN npm i -g pnpm
COPY pnpm-lock.yaml ./
COPY --from=preprocessor /tmp/deps.json ./package.json

RUN pnpm fetch --prod
RUN pnpm install --frozen-lockfile --offline --prod


###############################################################################
#  -------- build image with all package --------
# To cache all dependencies for build src
###############################################################################
FROM base_image as build_image

WORKDIR /usr/src/app

# install dev package only (already have prod dependencies in base image)
RUN pnpm fetch
RUN pnpm install --frozen-lockfile --offline

COPY . .

RUN pnpm run build


###############################################################################
#  -------- production image --------
# prepare and run code
###############################################################################
FROM node:18-alpine as production_image

WORKDIR /usr/src/app

COPY --from=base_image /usr/src/app/node_modules ./node_modules
COPY package.json ./
COPY --from=build_image /usr/src/app/dist ./dist

EXPOSE 3000

CMD [ "node", "dist/src/main"]