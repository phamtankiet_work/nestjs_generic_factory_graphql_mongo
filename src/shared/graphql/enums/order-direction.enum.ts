import { registerEnumType } from '@nestjs/graphql';

export enum OrderDirection {
  ASC = 1,
  DESC = -1,
}

registerEnumType(OrderDirection, {
  name: 'OrderDirection',
  description: 'The orderBy directions',
  valuesMap: {
    ASC: {
      description: 'ascending',
    },
    DESC: {
      description: 'descending',
    },
  },
});
