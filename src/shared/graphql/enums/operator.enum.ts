import { registerEnumType } from '@nestjs/graphql';

export enum Operator {
  /** Less Than */
  LT = 'LT',
  /** Less Than or Equal */
  LTE = 'LTE',
  /** Greater Than */
  GT = 'GT',
  /** Greater Than or Equal */
  GTE = 'GTE',
  /** Not Equal !=, or {value} not exist */
  NE = 'NE',
  /** Equal = */
  EQ = 'EQ',
  /** In (value of field equal value in array) */
  IN = 'IN',
  /** Not in (value not in array or value not exist) */
  NIN = 'NIN',
  /** String starts with */
  STARTS_WITH = 'STARTS_WITH',
  /** String ends with */
  ENDS_WITH = 'ENDS_WITH',
  /** String contains */
  CONTAINS = 'CONTAINS',
}

registerEnumType(Operator, {
  name: 'Operator',
  description: 'Possible filter operators',
  valuesMap: {
    LT: {
      description: '{value} Less Than <',
    },
    LTE: {
      description: '{value} Less Than or Equal <=',
    },
    GT: {
      description: '{value} Greater Than >',
    },
    GTE: {
      description: '{value} Greater Than or Equal >=',
    },
    NE: {
      description: '{value} Not Equal !=, or {value} not exist',
    },
    EQ: {
      description: '{value} Equal =',
    },
    IN: {
      description: 'In (value of field equal value in array)',
    },
    NIN: {
      description: 'Not in (value not in array or value not exist)',
    },
    STARTS_WITH: {
      description: 'String starts with {value}',
    },
    ENDS_WITH: {
      description: 'String ends with {value}',
    },
    CONTAINS: {
      description: 'String contains {value}',
    },
  },
});
