import { ConfigService } from '@nestjs/config';
import { ApolloDriverConfig } from '@nestjs/apollo';
import { Injectable, Logger } from '@nestjs/common';
import { GqlOptionsFactory } from '@nestjs/graphql';
import { GraphqlConfig } from '../config';
import { ApolloServerPluginLandingPageLocalDefault } from '@apollo/server/plugin/landingPage/default';

@Injectable()
export class GraphqlService implements GqlOptionsFactory {
  constructor(private configService: ConfigService) {}
  createGqlOptions(): ApolloDriverConfig {
    const graphqlConfig = this.configService.get<GraphqlConfig>('graphql');
    return {
      // schema options
      autoSchemaFile: graphqlConfig.schemaDestination,
      sortSchema: graphqlConfig.sortSchema,
      path: graphqlConfig.endPoint,
      buildSchemaOptions: {
        numberScalarMode: 'integer',
      },

      // subscription
      installSubscriptionHandlers: true,

      //
      //   formatError(error) {
      //     Logger.error(
      //       error.message,
      //       {
      //         metadata: {
      //           stack: error.stack,
      //           name: error.name,
      //           message: error.message,
      //           extensions: error.extensions,
      //         },
      //       },
      //       'GRAPHQL-API',
      //     );

      //     return {
      //       message: error.message,
      //       code: error.extensions && error.extensions.code,
      //       locations: error.locations,
      //       path: error.path,
      //     };
      //   },
      //   formatResponse: (response) => {
      //     return response;
      //   },

      //
      //   debug: graphqlConfig.debug,
      playground: false,
      plugins: [ApolloServerPluginLandingPageLocalDefault()],
      //   bodyParserConfig: { limit: '50mb' },
      introspection: true,
      context: ({ req }) => ({ req }),
    };
  }
}
