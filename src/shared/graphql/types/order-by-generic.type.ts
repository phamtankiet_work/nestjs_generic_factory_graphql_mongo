import { Type } from '@nestjs/common';
import { InputType, Field } from '@nestjs/graphql';
import { IsNotEmpty } from 'class-validator';
import { OrderDirection } from '../enums';

export interface IOrderBy<> {
  readonly field: any;
  direction: OrderDirection;
}
export function OrderByGeneric<TItem>(TItemEnum: any, name: string): any {
  @InputType(`OrderBy${name}`, { isAbstract: true })
  abstract class OrderByGenericClass implements IOrderBy {
    @Field(() => TItemEnum)
    @IsNotEmpty()
    readonly field: TItem;

    @Field(() => OrderDirection)
    @IsNotEmpty()
    readonly direction: OrderDirection;
  }
  return OrderByGenericClass;
}
