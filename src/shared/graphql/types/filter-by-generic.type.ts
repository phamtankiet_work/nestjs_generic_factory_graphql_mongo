import { Type } from '@nestjs/common';
import { InputType, Field } from '@nestjs/graphql';
import { IsNotEmpty, MinLength } from 'class-validator';
import { Operator } from '../enums';
import { Schema } from 'mongoose';

export interface IFilterBy {
  readonly field: any;
  readonly operator: Operator;
  readonly value: string;
}
export function FilterByGeneric<TItem>(TItemEnum: any, name: string): any {
  @InputType(`FilterBy${name}`, { isAbstract: true })
  abstract class FilterByGenericClass implements IFilterBy {
    @Field((type) => TItemEnum)
    @IsNotEmpty()
    readonly field: TItem;

    @Field((type) => Operator)
    @IsNotEmpty()
    readonly operator: Operator;

    @Field((type) => String)
    @MinLength(0)
    readonly value: string;
  }
  return FilterByGenericClass;
}
