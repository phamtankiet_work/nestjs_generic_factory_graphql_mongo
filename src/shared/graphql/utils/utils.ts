import { IFilterBy, IOrderBy } from '../types';
import { Operator } from '../enums';
import { Types } from 'mongoose';
import { lodash } from '../../../libs';

interface FilterByResult {
  [operator: string]: string;
}

type searchValue = string | number | boolean | Types.ObjectId;

export function getFilterByQuery(filterBys: IFilterBy[]) {
  const filterObj = {};
  if (Array.isArray(filterBys)) {
    filterBys.forEach((filter) => {
      let value: searchValue = filter.value;
      // check if filter.field is _id
      if (filter.field === '_id' || lodash.endsWith(filter.field, 'Id')) {
        value = new Types.ObjectId(filter.value);
      }
      filterObj[filter.field] = getFilterByOperator(filter.operator, value);
    });
  }
  return filterObj;
}

function getFilterByOperator(
  operator: Operator,
  searchValue: searchValue,
): FilterByResult {
  let operation;

  switch (operator) {
    case Operator.EQ:
      operation = { $eq: searchValue };
      break;
    case Operator.NE:
      operation = { $ne: searchValue };
      break;
    case Operator.GTE:
      operation = { $gte: searchValue };
      break;
    case Operator.GT:
      operation = { $gt: searchValue };
      break;
    case Operator.LTE:
      operation = { $lte: searchValue };
      break;
    case Operator.LT:
      operation = { $lt: searchValue };
      break;
    case Operator.IN:
      const inValues =
        typeof searchValue === 'string' ? searchValue.split(',') : searchValue;
      operation = { $in: inValues || [] };
      break;
    case Operator.NIN:
      const notInValues =
        typeof searchValue === 'string' ? searchValue.split(',') : searchValue;
      operation = { $nin: notInValues || [] };
      break;
    case Operator.ENDS_WITH:
      operation = { $regex: new RegExp(`.*${searchValue}$`, 'i') };
      break;
    case Operator.STARTS_WITH:
      operation = { $regex: new RegExp(`^${searchValue}.*`, 'i') };
      break;
    case Operator.CONTAINS:
    default:
      operation = { $regex: new RegExp(`.*${searchValue}.*`, 'i') };
      break;
  }
  return operation;
}

export function getOrderByQuery(orderBys: IOrderBy[]) {
  const orderObj = {};
  if (Array.isArray(orderBys)) {
    orderBys.forEach((orderBy) => {
      orderObj[orderBy.field] = orderBy.direction;
    });
  }
  return orderObj;
}
