import { IsISO8601, Validator } from 'class-validator';
import { CustomScalar, Scalar } from '@nestjs/graphql';
import { Kind, ValueNode, GraphQLError } from 'graphql';
import { Logger } from '@nestjs/common';
import moment from 'moment';

@Scalar('Date', (type) => Date)
export class DateScalar implements CustomScalar<string, Date> {
  description = 'Date custom scalar type, example 2023-02-10T03:17:58Z';

  private validator: Validator;

  constructor() {
    this.validator = new Validator();
  }

  /**
   *
   * @param value value from the client
   * @returns
   */
  parseValue(value: string): Date {
    if (value) {
      return new Date(value);
    }
    return null;
  }

  /**
   *
   * @param value
   * @returns {string} value sent to the client
   */
  serialize(value: Date): string {
    return value.toISOString();
  }

  parseLiteral(ast: ValueNode): Date {
    if (ast.kind !== Kind.STRING) {
      throw new GraphQLError(
        `Can only validate strings as Date but got a: ${ast.kind}`,
      );
    }
    if (moment(ast.value, 'YYYY-MM-DDTHH:mm:ssZ', true)) {
      throw new GraphQLError(
        'Date cannot represent an invalid ISO-8601 Date string',
      );
    }
    return new Date(ast.value);
  }
}
