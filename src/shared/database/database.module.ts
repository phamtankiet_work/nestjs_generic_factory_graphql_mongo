import { Module } from '@nestjs/common';
import { databaseImports } from './database.providers';

@Module({
  imports: [...databaseImports],
  providers: [],
  exports: [],
})
export class DatabaseModule {}
