import { Logger } from '@nestjs/common';
import { MongooseModule } from '@nestjs/mongoose';
import { config, DATABASE_CONNECTION } from '../config';

function connectionFactory(connection: any, name: string) {
  const logger = new Logger(name);
  if (connection.readyState === 1) {
    logger.log('DB connected');
  }
  connection.on('connected', () => {
    logger.log('is connected');
  });
  connection.on('disconnected', () => {
    logger.warn('DB disconnected');
  });
  connection.on('error', (error) => {
    logger.error('DB connection failed! for error: ', error);
  });
  return connection;
}
export const databaseImports = [
  MongooseModule.forRoot(config.database.uri, {
    connectionName: DATABASE_CONNECTION.MAIN_DB,
    ...config.database.options,
    connectionFactory: connectionFactory,
  }),
];
