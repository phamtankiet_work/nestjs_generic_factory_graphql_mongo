import {
  BadGatewayException,
  Logger,
  NotFoundException,
  Type,
} from '@nestjs/common';
import { classToPlain } from 'class-transformer';
import { Document, Model, Types } from 'mongoose';
import { IBaseService } from '.';
import {
  IBaseEntity,
  ICreateInput,
  IGetAllInput,
  IPaginatedOutput,
  IUpdateInput,
} from '..';
import { lodash } from '../../../libs';
import {
  IFilterBy,
  IOrderBy,
  getFilterByQuery,
  getOrderByQuery,
} from '../../graphql';
import { tryParseToObjectId } from '../../utils';

export function BaseServiceFactory<
  SpecificEntity extends IBaseEntity,
  SpecificCreateInput extends ICreateInput,
  SpecificUpdateInput extends IUpdateInput,
  SpecificFilterBy extends IFilterBy,
  SpecificOrderBy extends IOrderBy,
  SpecificFetchAllInput extends IGetAllInput<SpecificFilterBy, SpecificOrderBy>,
  SpecificPaginatedOutput extends IPaginatedOutput<SpecificEntity>,
>(
  entityType: Type<SpecificEntity>,
  createInputType: Type<SpecificCreateInput>,
  updateInputType: Type<SpecificUpdateInput>,
  specificFilterByType: Type<SpecificFilterBy>,
  specificOrderByByType: Type<SpecificOrderBy>,
  fetchAllInputType: Type<SpecificFetchAllInput>,
  paginatedOutputType: Type<SpecificPaginatedOutput>,
): Type<
  IBaseService<
    SpecificEntity,
    SpecificCreateInput,
    SpecificUpdateInput,
    SpecificFilterBy,
    SpecificOrderBy,
    SpecificFetchAllInput,
    SpecificPaginatedOutput
  >
> {
  class BaseService
    implements
      IBaseService<
        SpecificEntity,
        SpecificCreateInput,
        SpecificUpdateInput,
        SpecificFilterBy,
        SpecificOrderBy,
        SpecificFetchAllInput,
        SpecificPaginatedOutput
      >
  {
    constructor(
      private readonly logger: Logger,
      private readonly model: Model<SpecificEntity>,
    ) {
      this.logger = logger;
      this.model = model;
    }
    async create(data: SpecificCreateInput): Promise<Document<SpecificEntity>> {
      try {
        return new Promise<Document<SpecificEntity>>((resolve, reject) => {
          this.model
            .create(new entityType(data))
            .then((created) => resolve(created.toObject()))
            .catch((err) => reject(err));
        });
      } catch (error) {
        this.logger.error(error);
        throw new BadGatewayException(error);
      }
    }

    async fetchAll(fetchAllInput: SpecificFetchAllInput): Promise<any> {
      try {
        const filterBy = lodash.get(fetchAllInput, 'filterBy');
        const orderBy = lodash.get(fetchAllInput, 'orderBy');
        const search = lodash.get(fetchAllInput, 'search');
        const limit = lodash.get(fetchAllInput, 'limit') || 10;
        const offset = lodash.get(fetchAllInput, 'offset') || 0;
        const page = lodash.get(fetchAllInput, 'page') || 1;
        const skip = offset || (page - 1) * limit || 0;

        // filter function
        const queryInput: object = getFilterByQuery(filterBy);
        const query = this.model.find();

        // search function
        if (search) {
          lodash.set(queryInput, '$text.$search', search);
          query.select({ score: { $meta: 'textScore' } });
          query.sort({ score: { $meta: 'textScore' } });
        }
        query.setQuery(queryInput);

        // count function
        const countQuery = this.model.find().merge(query);

        // orderBy function
        query.sort(getOrderByQuery(orderBy));

        query.limit(limit);
        query.skip(skip);

        return await Promise.all([
          query.lean().exec(),
          countQuery.count(),
        ]).then((res) => {
          const [items, totalResult]: [items: any[], totalResult: number] = res;
          const itemsPerPage: number = limit;
          const totalPages: number = Math.ceil(totalResult / itemsPerPage);
          const hasFirstPage: boolean = page > 1;
          const hasLastPage: boolean = page > totalPages;
          const firstPage = 1;
          const lastPage: number = totalPages;
          const hasPrevPage: boolean = page - 1 >= 0;
          const hasNextPage: boolean = page < totalPages;
          const prevPage: string = hasPrevPage ? page - 1 : page;
          const nextPage: string = hasNextPage ? page + 1 : page;
          return {
            items: items,
            pageInfo: {
              firstPage: firstPage,
              lastPage: lastPage,
              hasFirstPage: hasFirstPage,
              hasLastPage: hasLastPage,
              prevPage: prevPage,
              nextPage: nextPage,
              hasPrevPage: hasPrevPage,
              hasNextPage: hasNextPage,
              page: page,
              itemsPerPage: itemsPerPage,
              totalPages: totalPages,
              totalItems: totalResult,
            },
          };
        });
      } catch (error) {
        this.logger.error(error);
        throw new BadGatewayException(error);
      }
    }

    async findAll(options: object): Promise<Document<SpecificEntity[]>> {
      try {
        return this.model.find(options).lean();
      } catch (error) {
        this.logger.error(error);
        throw new BadGatewayException(error);
      }
    }

    async findOne(_id: Types.ObjectId): Promise<Document<SpecificEntity>> {
      try {
        _id = new Types.ObjectId(_id);
        return await this.model.findOne({ _id: _id }).lean();
      } catch (error) {
        throw new BadGatewayException(error);
      }
    }
    async findById(_id: Types.ObjectId): Promise<Document<SpecificEntity>> {
      try {
        _id = new Types.ObjectId(_id);
        return await this.model.findById(_id).lean();
      } catch (error) {
        this.logger.error(error);
        throw new BadGatewayException(error);
      }
    }

    async update(
      _id: Types.ObjectId,
      data: SpecificUpdateInput,
    ): Promise<Document<SpecificEntity>> {
      try {
        const updateData = classToPlain(data) as any;
        updateData['updatedAt'] = new Date();
        await this.ensureExists(_id);
        await this.model.updateOne({ _id: _id }, updateData);
        return await this.findById(_id);
      } catch (error) {
        this.logger.error(error);
        throw new BadGatewayException(error);
      }
    }

    async deleteOneById(
      _id: Types.ObjectId,
    ): Promise<Document<SpecificEntity>> {
      try {
        await this.ensureExists(_id);
        return this.model.findOneAndDelete({ _id: _id }).lean();
      } catch (error) {
        this.logger.error(error);
        throw new BadGatewayException(error);
      }
    }
    // write a function check if entity exist
    async isExists(id: Types.ObjectId): Promise<boolean> {
      try {
        return (await this.model.countDocuments({ _id: id })) > 0;
      } catch (error) {
        this.logger.error(error);
        throw new BadGatewayException(error);
      }
    }
    // ensure entity exist and throw error if not
    // write jsdoc for function
    /**
     * Finds a specific entity by its ID and returns it, or throws an error if it does not exist.
     *
     * @async
     * @function ensureExists
     * @memberof module:EntityService
     * @param {Types.ObjectId} _id - The ID of the entity to look up.
     * @throws {NotFoundException} If no entity with the specified ID is found.
     * @throws {Error} If there is an error while retrieving the entity.
     * @returns {Promise<SpecificEntity>} The entity with the specified ID.
     */

    async ensureExists(_id: Types.ObjectId): Promise<SpecificEntity> {
      try {
        _id = tryParseToObjectId(_id);
        const entity = await this.model.findById(_id);
        if (!entity) {
          throw new NotFoundException(
            `${entityType.name} with id ${_id.toString()} not found`,
          );
        }
        return entity;
      } catch (error) {
        this.logger.error('ensureExist: ' + error.message);
        throw error;
      }
    }
    // write a function to count document by query
    async count(query: object): Promise<number> {
      try {
        return await this.model.countDocuments(query);
      } catch (error) {
        this.logger.error(error);
        throw new BadGatewayException(error);
      }
    }
  }
  return BaseService;
}
