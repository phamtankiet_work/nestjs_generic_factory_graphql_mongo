import mongoose, { Document, Types } from 'mongoose';
import { ICreateInput, IUpdateInput } from '../dto';
import { IBaseEntity } from '../entities';
import { IGetAllInput } from '../dto/get-all.input.interface';
import { IPaginatedOutput } from '../dto';
import { IFilterBy, IOrderBy } from '../../graphql';

export interface IBaseService<
  SpecificEntity extends IBaseEntity,
  SpecificCreateInput extends ICreateInput,
  SpecificUpdateInput extends IUpdateInput,
  SpecificFilterBy extends IFilterBy,
  SpecificOrderBy extends IOrderBy,
  SpecificFetchAllInput extends IGetAllInput<SpecificFilterBy, SpecificOrderBy>,
  SpecificPaginatedOutput extends IPaginatedOutput<SpecificEntity>,
> {
  fetchAll(fetchAllInput: SpecificFetchAllInput): Promise<any>;
  findAll(options: object): Promise<Document<SpecificEntity[]>>;
  findOne(_id: mongoose.Types.ObjectId): Promise<Document<SpecificEntity>>;
  findById(_id: Types.ObjectId): Promise<Document<SpecificEntity>>;
  update(
    _id: Types.ObjectId,
    entity: SpecificUpdateInput,
  ): Promise<Document<SpecificEntity>>;
  create(entity: SpecificCreateInput): Promise<Document<SpecificEntity>>;
  deleteOneById(_id: Types.ObjectId): Promise<Document<SpecificEntity>>;
  ensureExists(_id: Types.ObjectId): Promise<SpecificEntity>;
  count(query: object): Promise<number>;
}
