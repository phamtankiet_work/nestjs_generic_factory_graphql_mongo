import { Field, ID, InterfaceType, ObjectType } from '@nestjs/graphql';
import { Prop } from '@nestjs/mongoose';
import { Transform } from 'class-transformer';
import mongoose, { Types } from 'mongoose';
import { IBaseEntity } from './base.inteface.entity';

@InterfaceType()
@ObjectType({ isAbstract: true })
export abstract class BaseEntity implements IBaseEntity {
  @Transform((value) => value.obj._id.toString())
  @Prop({ type: mongoose.Schema.Types.ObjectId })
  @Field(() => ID, { name: 'id', nullable: false, description: 'ID' })
  readonly _id: Types.ObjectId;

  @Prop(Date)
  @Field(() => Date, {
    description: 'Identifies the date and time when the object was created.',
  })
  readonly createdAt: Date;

  @Prop(Date)
  @Field(() => Date, {
    description:
      'Identifies the date and time when the object was last updated.',
  })
  readonly updatedAt: Date;

  constructor(base?: Partial<IBaseEntity>) {
    if (base) {
      Object.assign(this, base);
    }
    this._id = this._id || new Types.ObjectId();
    this.createdAt = this.createdAt || new Date();
    this.updatedAt = new Date();
  }
}

export enum BaseEntityField {
  id = '_id',
  createdAt = 'createdAt',
  updatedAt = 'updatedAt',
}
