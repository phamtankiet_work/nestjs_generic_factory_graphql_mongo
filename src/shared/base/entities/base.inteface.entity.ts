import { Types } from 'mongoose';

export interface IBaseEntity {
  readonly _id: Types.ObjectId;
  readonly createdAt: Date;
  readonly updatedAt: Date;
}
