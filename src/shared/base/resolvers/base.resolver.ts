import { Type } from '@nestjs/common';
import { Args, ID, Mutation, Query, Resolver } from '@nestjs/graphql';
import { Document, Types } from 'mongoose';
import {
  IBaseEntity,
  IBaseResolver,
  IBaseService,
  ICreateInput,
  IGetAllInput,
  IPaginatedOutput,
  IUpdateInput,
} from '..';
import { IFilterBy, IOrderBy } from '../../graphql';
import { tryGetId, tryParseToObjectId } from '../../utils';

export function BaseResolverFactory<
  SpecificEntity extends IBaseEntity,
  SpecificCreateInput extends ICreateInput,
  SpecificUpdateInput extends IUpdateInput,
  SpecificFilterBy extends IFilterBy,
  SpecificOrderBy extends IOrderBy,
  SpecificFetchAllInput extends IGetAllInput<SpecificFilterBy, SpecificOrderBy>,
  SpecificPaginatedOutput extends IPaginatedOutput<SpecificEntity>,
  SpecificService extends IBaseService<
    SpecificEntity,
    SpecificCreateInput,
    SpecificUpdateInput,
    SpecificFilterBy,
    SpecificOrderBy,
    SpecificFetchAllInput,
    SpecificPaginatedOutput
  >,
>(
  entityType: Type<SpecificEntity>,
  createInputType: Type<SpecificCreateInput>,
  updateInputType: Type<SpecificUpdateInput>,
  fetchAllInputType: Type<SpecificFetchAllInput>,
  paginatedOutputType: Type<SpecificPaginatedOutput>,
): Type<
  IBaseResolver<
    SpecificEntity,
    SpecificCreateInput,
    SpecificUpdateInput,
    SpecificFilterBy,
    SpecificOrderBy,
    SpecificFetchAllInput,
    SpecificPaginatedOutput,
    SpecificService
  >
> {
  @Resolver({ isAbstract: true })
  class BaseResolver
    implements
      IBaseResolver<
        SpecificEntity,
        SpecificCreateInput,
        SpecificUpdateInput,
        SpecificFilterBy,
        SpecificOrderBy,
        SpecificFetchAllInput,
        SpecificPaginatedOutput,
        SpecificService
      >
  {
    readonly service: SpecificService;

    constructor(service: SpecificService) {
      this.service = service;
    }

    // ====== Queries =======
    @Query((type) => paginatedOutputType, {
      name: `getAll${entityType.name}`,
    })
    async fetchAll(
      @Args({
        type: () => fetchAllInputType,
        name: `input`,
        nullable: true,
      })
      fetchAllInput: SpecificFetchAllInput,
    ): Promise<SpecificPaginatedOutput> {
      return await this.service.fetchAll(fetchAllInput);
    }

    @Query(() => entityType, {
      name: `getOne${entityType.name}`,
      nullable: true,
    })
    async findById(
      @Args({ name: 'id', type: () => ID }) _id: Types.ObjectId,
    ): Promise<Document<SpecificEntity>> {
      return await this.service.findById(_id);
    }

    // ====== Mutations =======
    @Mutation(() => entityType, {
      name: `create${entityType.name}`,
    })
    async create(
      @Args({
        type: () => createInputType,
        name: `input`,
      })
      createInput: SpecificCreateInput,
    ): Promise<Document<SpecificEntity>> {
      return await this.service.create(createInput);
    }

    @Mutation(() => entityType, {
      name: `update${entityType.name}`,
    })
    async update(
      @Args({
        type: () => updateInputType,
        name: `input`,
      })
      updateInput: SpecificUpdateInput,
    ): Promise<Document<SpecificEntity>> {
      return await this.service.update(tryGetId(updateInput), updateInput);
    }
    @Mutation(() => entityType, {
      name: `delete${entityType.name}`,
    })
    async deleteOneById(
      @Args({ name: 'id', type: () => ID }) _id: Types.ObjectId,
    ): Promise<Document<SpecificEntity>> {
      return await this.service.deleteOneById(tryParseToObjectId(_id));
    }
    // ====== Resolvers =======
  }
  return BaseResolver;
}
