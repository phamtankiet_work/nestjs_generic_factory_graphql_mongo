import { Document, Types } from 'mongoose';
import {
  IBaseEntity,
  IBaseService,
  ICreateInput,
  IGetAllInput,
  IPaginatedOutput,
  IUpdateInput,
} from '..';
import { IFilterBy, IOrderBy } from '../../graphql';

export interface IBaseResolver<
  SpecificEntity extends IBaseEntity,
  SpecificCreateInput extends ICreateInput,
  SpecificUpdateInput extends IUpdateInput,
  SpecificFilterBy extends IFilterBy,
  SpecificOrderBy extends IOrderBy,
  SpecificFetchAllInput extends IGetAllInput<SpecificFilterBy, SpecificOrderBy>,
  SpecificPaginatedOutput extends IPaginatedOutput<SpecificEntity>,
  SpecificService extends IBaseService<
    SpecificEntity,
    SpecificCreateInput,
    SpecificUpdateInput,
    SpecificFilterBy,
    SpecificOrderBy,
    SpecificFetchAllInput,
    SpecificPaginatedOutput
  >,
> {
  readonly service: SpecificService;

  fetchAll(
    fetchAllInput: SpecificFetchAllInput,
  ): Promise<SpecificPaginatedOutput>;
  findById(_id: Types.ObjectId): Promise<Document<SpecificEntity>>;
  create(createInput: SpecificCreateInput): Promise<Document<SpecificEntity>>;
  update(updateInput: SpecificUpdateInput): Promise<Document<SpecificEntity>>;
  deleteOneById(_id: Types.ObjectId): Promise<Document<SpecificEntity>>;
}
