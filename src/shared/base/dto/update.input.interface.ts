import { Types } from 'mongoose';
export interface IUpdateInput {
  _id: Types.ObjectId;
}
