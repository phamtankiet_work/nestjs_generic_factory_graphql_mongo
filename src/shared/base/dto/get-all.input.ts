import { Type } from '@nestjs/common';
import { Field, InputType, Int } from '@nestjs/graphql';
import { Transform } from 'class-transformer';
import { IsNumber, IsOptional, Min } from 'class-validator';
import { IFilterBy, IOrderBy } from '../../graphql';
import { IGetAllInput } from './get-all.input.interface';

export function getAllInputFactory<
  SpecificFilterBy extends IFilterBy,
  SpecificOrderBy extends IOrderBy,
>(
  specificFilterByType: SpecificFilterBy,
  specificOrderByType: SpecificOrderBy,
): Type<IGetAllInput<SpecificFilterBy, SpecificOrderBy>> {
  @InputType({ isAbstract: true })
  class BaseFetchAllInput
    implements IGetAllInput<SpecificFilterBy, SpecificOrderBy>
  {
    @Field(() => Int, {
      description:
        'Limit number of results to be returned, defaults to 10, use with page or offset, should not use both',
      nullable: true,
    })
    @IsNumber()
    @IsOptional()
    @Min(0)
    @Transform(({ value }) => parseInt(value, 10), { toClassOnly: true })
    limit: number;

    @Field(() => Int, {
      description:
        'Offset from number of results to be returned, defaults to 0',
      nullable: true,
    })
    @IsNumber()
    @IsOptional()
    @Min(0)
    @Transform(({ value }) => parseInt(value, 10), { toClassOnly: true })
    offset: number;

    @Field(() => Int, {
      description:
        'Page number to start from, defaults to 1, lower priority than offset',
      nullable: true,
    })
    @IsNumber()
    @IsOptional()
    @Min(1)
    @Transform(({ value }) => parseInt(value, 10), { toClassOnly: true })
    page: number;

    @Field((type) => [specificFilterByType])
    @IsOptional()
    filterBy?: SpecificFilterBy[];

    @Field((type) => [specificOrderByType])
    @IsOptional()
    orderBy?: SpecificOrderBy[];

    @Field(() => String, {
      description:
        'Full text search on many field, higher priority than filterBy, and orderBy args',
      nullable: true,
    })
    @IsOptional()
    search?: string;
  }
  return BaseFetchAllInput;
}
