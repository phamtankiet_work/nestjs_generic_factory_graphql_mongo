import { Type } from '@nestjs/common';
import { InputType, OmitType } from '@nestjs/graphql';
import { IBaseEntity } from '../entities';
import { ICreateInput } from './create.input.interface';

export function CreateInputFactory<SpecificEntity extends IBaseEntity>(
  specificEntityType: Type<SpecificEntity>,
): Type<ICreateInput> {
  const OmitClass = OmitType(
    specificEntityType,
    ['_id', 'createdAt', 'updatedAt'] as const,
    InputType,
  );
  @InputType({ isAbstract: true })
  // eslint-disable-next-line @typescript-eslint/ban-ts-comment
  // @ts-ignore
  class BaseCreateInput extends OmitClass implements ICreateInput {}
  return BaseCreateInput;
}
