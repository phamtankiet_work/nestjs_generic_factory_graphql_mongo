import { Type } from '@nestjs/common';
import { InputType, OmitType } from '@nestjs/graphql';
import { Types } from 'mongoose';
import { IBaseEntity } from '../entities';
import { IUpdateInput } from './update.input.interface';

export function UpdateInputFactory<SpecificEntity extends IBaseEntity>(
  specificEntityType: Type<SpecificEntity>,
): Type<IUpdateInput> {
  const OmitClass = OmitType(
    specificEntityType,
    ['createdAt', 'updatedAt'] as const,
    InputType,
  );
  @InputType({ isAbstract: true })
  // eslint-disable-next-line @typescript-eslint/ban-ts-comment
  // @ts-ignore
  class BaseUpdateInput extends OmitClass implements IUpdateInput {
    readonly _id: Types.ObjectId;
  }
  return BaseUpdateInput;
}
