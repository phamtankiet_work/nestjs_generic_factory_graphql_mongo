import { IFilterBy, IOrderBy } from '../../graphql';

export interface IGetAllInput<
  SpecificFilterBy extends IFilterBy,
  SpecificOrderBy extends IOrderBy,
> {
  readonly limit?: number;
  readonly offset?: number;
  readonly page?: number;
  readonly filterBy?: SpecificFilterBy[];
  readonly orderBy?: SpecificOrderBy[];
  readonly search?: string;
}
