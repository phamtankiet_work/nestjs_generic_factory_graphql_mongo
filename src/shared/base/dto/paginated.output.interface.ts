import { IBaseEntity } from '../entities';

export interface IPaginatedOutput<SpecificEntity extends IBaseEntity> {
  items: Array<SpecificEntity>;
  pageInfo: IPageInfo;
}

export interface IPageInfo {
  firstPage: number;
  lastPage: number;
  hasFirstPage: boolean;
  hasLastPage: boolean;
  nextPage: number;
  prevPage: number;

  hasPrevPage: boolean;
  hasNextPage: boolean;
  page: number;
  itemsPerPage: number;
  totalPages: number;
  totalItems: number;
}
