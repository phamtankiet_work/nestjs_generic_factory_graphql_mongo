import { Type } from '@nestjs/common';
import { Field, Int, ObjectType } from '@nestjs/graphql';
import { IBaseEntity } from '../entities';
import { IPageInfo, IPaginatedOutput } from './paginated.output.interface';

@ObjectType()
class PageInfo implements IPageInfo {
  @Field(() => Int, {
    description: 'Number of the first page',
  })
  firstPage: number;

  @Field(() => Int, {
    description: 'Number of the last page',
  })
  lastPage: number;

  @Field(() => Boolean, {
    description: 'Indicates whether the first page is exists',
  })
  hasFirstPage: boolean;

  @Field(() => Boolean, {
    description: 'Indicates whether the last page is exists',
  })
  hasLastPage: boolean;

  @Field(() => Int, {
    description: 'Number of the previous page',
  })
  prevPage: number;

  @Field(() => Int, {
    description: 'Number of the next page',
  })
  nextPage: number;

  @Field(() => Boolean, {
    description: 'Indicates whether the previous page is exists',
  })
  hasPrevPage: boolean;

  @Field(() => Boolean, {
    description: 'Indicates whether the next page is exists',
  })
  hasNextPage: boolean;

  @Field(() => Number, {
    description: 'Number of the current page',
  })
  page: number;

  @Field(() => Number, {
    description: 'Number of items per page',
  })
  itemsPerPage: number;

  @Field(() => Number, {
    description: 'Total number of pages',
  })
  totalPages: number;

  @Field(() => Number, { description: 'Total number of items' })
  totalItems: number;
}
export function PaginatedInputFactory<SpecificEntity extends IBaseEntity>(
  specificEntityType: Type<SpecificEntity>,
): Type<IPaginatedOutput<SpecificEntity>> {
  @ObjectType({ isAbstract: true })
  class PaginatedOutput implements IPaginatedOutput<SpecificEntity> {
    @Field(() => [specificEntityType], {
      description: 'List of item result',
      nullable: false,
    })
    items: Array<SpecificEntity> = [];

    @Field(() => PageInfo, {
      description: 'List of item result',
      nullable: false,
    })
    pageInfo: IPageInfo;
  }
  return PaginatedOutput;
}
