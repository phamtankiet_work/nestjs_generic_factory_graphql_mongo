export enum Constants {}

export enum DATABASE_CONNECTION {
  MAIN_DB = 'MAINDB',
}

export enum PASSPORT_STRATEGY {
  JWT = 'jwt',
}

export enum MODEL_NAME {
  AUTHOR = 'author',
  BOOK = 'book',
}
