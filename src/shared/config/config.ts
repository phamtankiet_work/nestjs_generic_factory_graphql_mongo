import * as dotenv from 'dotenv';
dotenv.config();

import type { Config, PackageJson } from './config.interface';

import * as pjson from '../../../package.json';
const PackageJson: PackageJson = pjson;

const config: Config = {
  app: {
    nodeEnv: process.env.NODE_ENV,
    name: PackageJson.name,
    version: PackageJson.version,
    description: PackageJson.description,
    selfDomain: process.env.SELF_DOMAIN,
    baseDomain: process.env.BASE_DOMAIN,
    port: parseInt(process.env.PORT, 10) || 3000,
    requestIpTokenHeader: process.env.APP_HEADER_REQUEST_IP || 'x-request-id',
    forwardedForTokenHeader:
      process.env.APP_HEADER_FORWARD_IP || 'X-Forwarded-For',
  },
  database: {
    uri: process.env.MONGO_URI_MAIN,
    options: {
      useNewUrlParser: true,
      socketTimeoutMS: 30000,
      useUnifiedTopology: true,
    },
  },
  cors: {
    enabled: true,
  },
  // swagger: {
  //   enabled: true,
  //   title: 'Nestjs FTW',
  //   description: 'The nestjs API description',
  //   version: '1.5',
  //   path: 'api',
  // },
  graphql: {
    endPoint: process.env.END_POINT || 'api/graphql',
    playgroundEnabled: process.env.NODE_ENV === 'development',
    debug: process.env.NODE_ENV === 'development',
    schemaDestination: process.env.GRAPHQL_ENDPOINT || './dist/schema.graphql',
    sortSchema: false,
    introspection: process.env.NODE_ENV === 'development',
  },
  security: {
    secret: process.env.JWT_SECRET,
    expiresIn: process.env.JWT_EXPIRE_IN || '5m',
    refreshIn: process.env.JWT_REFRESH_IN || '30d',
    bcryptSaltOrRound: 10,
  },
  helmet: {
    contentSecurityPolicy:
      process.env.NODE_ENV === 'production' ? undefined : false,
  },
};

export default (): Config => config;
export { config };
