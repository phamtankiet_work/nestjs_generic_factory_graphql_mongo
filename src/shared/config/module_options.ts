import { ConfigModuleOptions } from '@nestjs/config/dist/interfaces';

import configuration from './config';

export const configModuleOptions: ConfigModuleOptions = {
  envFilePath: '.env',
  isGlobal: true,
  cache: true,
  load: [configuration],
};
