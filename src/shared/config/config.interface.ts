export interface Config {
  app: AppConfig;
  database: DatabaseConfig;
  cors: CorsConfig;
  // swagger: SwaggerConfig;
  graphql: GraphqlConfig;
  security: SecurityConfig;
  helmet: HelmetConfig;
}

export interface AppConfig {
  nodeEnv: string;
  name: string;
  version: string;
  description: string;
  selfDomain: string;
  baseDomain: string;
  port: number;
  requestIpTokenHeader: string;
  forwardedForTokenHeader: string;
}
export interface DatabaseConfig {
  uri: string;
  options?: {
    useNewUrlParser?: boolean;
    socketTimeoutMS?: number;
    useUnifiedTopology?: boolean;
  };
}
export interface CorsConfig {
  enabled: boolean;
}

// interface SwaggerConfig {
//   enabled: boolean;
//   title: string;
//   description: string;
//   version: string;
//   path: string;
// }

export interface GraphqlConfig {
  endPoint: string;
  playgroundEnabled: boolean;
  debug: boolean;
  schemaDestination: string;
  sortSchema: boolean;
  introspection: boolean;
}

export interface SecurityConfig {
  secret: string;
  expiresIn: string;
  refreshIn: string;
  bcryptSaltOrRound: string | number;
}

export interface PackageJson {
  name: string;
  version: string;
  description: string;
}

export interface HelmetConfig {
  contentSecurityPolicy: boolean;
}
