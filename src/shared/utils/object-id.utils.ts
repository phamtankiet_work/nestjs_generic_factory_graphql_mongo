import { ObjectId, Types } from 'mongoose';

export function tryGetId(object: any): Types.ObjectId {
  if (object._id) {
    return new Types.ObjectId(object._id);
  }
  if (object.id) {
    return new Types.ObjectId(object.id);
  }
}
// write method to try parse to objectId from id
export function tryParseToObjectId(
  id: string | Types.ObjectId,
): Types.ObjectId {
  if (id instanceof Types.ObjectId) {
    return id;
  }
  return new Types.ObjectId(id);
}
