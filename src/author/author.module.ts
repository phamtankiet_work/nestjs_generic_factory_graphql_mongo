import { Module } from '@nestjs/common';
import { AuthorService } from './author.service';
import { AuthorResolver } from './author.resolver';
import { AuthorSchema } from './entities';
import { MongooseModule } from '@nestjs/mongoose';
import { MODEL_NAME, DATABASE_CONNECTION } from '../shared';
import { SharedModule } from '../shared/shared.module';

@Module({
  imports: [
    SharedModule,
    MongooseModule.forFeature(
      [
        {
          name: MODEL_NAME.AUTHOR,
          schema: AuthorSchema,
          collection: MODEL_NAME.AUTHOR,
        },
      ],
      DATABASE_CONNECTION.MAIN_DB,
    ),
  ],
  providers: [AuthorResolver, AuthorService],
})
export class AuthorModule {}
