import { Logger } from '@nestjs/common';
import { Resolver } from '@nestjs/graphql';
import { BaseResolverFactory } from '../shared/base';
import { AuthorService } from './author.service';
import {
  CreateAuthorInput,
  FilterByAuthorFields,
  GetAllAuthorInput,
  OrderByAuthorFields,
  PaginatedAuthorOutput,
  UpdateAuthorInput,
} from './dto';
import { Author } from './entities';

@Resolver(() => Author)
export class AuthorResolver extends BaseResolverFactory<
  Author,
  CreateAuthorInput,
  UpdateAuthorInput,
  FilterByAuthorFields,
  OrderByAuthorFields,
  GetAllAuthorInput,
  PaginatedAuthorOutput,
  AuthorService
>(
  Author,
  CreateAuthorInput,
  UpdateAuthorInput,
  GetAllAuthorInput,
  PaginatedAuthorOutput,
) {
  private readonly logger: Logger = new Logger(AuthorResolver.name);
  constructor(private readonly AuthorService: AuthorService) {
    super(AuthorService);
  }
  // ====== Queries =======

  // ====== Mutations =======

  // ====== Resolvers =======
}
