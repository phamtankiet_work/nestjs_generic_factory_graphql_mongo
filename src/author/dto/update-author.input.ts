import { InputType, OmitType } from '@nestjs/graphql';
import { IUpdateInput } from '../../shared/base';
import { Author } from '../entities';

@InputType()
export class UpdateAuthorInput
  extends OmitType(Author, ['createdAt', 'updatedAt'] as const, InputType)
  implements IUpdateInput {}
