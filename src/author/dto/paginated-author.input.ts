import { ObjectType } from '@nestjs/graphql';
import { PaginatedInputFactory } from '../../shared/base';
import { Author } from '../entities';

@ObjectType()
export class PaginatedAuthorOutput extends PaginatedInputFactory<Author>(
  Author,
) {}
