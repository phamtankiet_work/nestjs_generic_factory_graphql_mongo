import { InputType, registerEnumType } from '@nestjs/graphql';
import { getAllInputFactory } from '../../shared/base';
import { FilterByGeneric, OrderByGeneric } from '../../shared/graphql';
import { AuthorFields } from '../entities';

registerEnumType(AuthorFields, {
  name: 'AuthorFields',
  description: 'Author fields',
});

export const FilterByAuthorFields = FilterByGeneric(
  AuthorFields,
  'AuthorFields',
);

export const OrderByAuthorFields = OrderByGeneric(AuthorFields, 'AuthorFields');
export type FilterByAuthorFields = InstanceType<typeof FilterByAuthorFields>;
export type OrderByAuthorFields = InstanceType<typeof OrderByAuthorFields>;

@InputType()
export class GetAllAuthorInput extends getAllInputFactory<
  FilterByAuthorFields,
  OrderByAuthorFields
>(FilterByAuthorFields, OrderByAuthorFields) {}
