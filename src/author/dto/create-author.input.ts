import { InputType, OmitType } from '@nestjs/graphql';
import { ICreateInput } from '../../shared/base';
import { Author } from '../entities';

@InputType()
export class CreateAuthorInput
  extends OmitType(
    Author,
    ['_id', 'createdAt', 'updatedAt'] as const,
    InputType,
  )
  implements ICreateInput {}
