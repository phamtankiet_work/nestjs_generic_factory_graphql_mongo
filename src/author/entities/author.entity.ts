import { Field, ObjectType } from '@nestjs/graphql';
import { Prop, Schema, SchemaFactory } from '@nestjs/mongoose';
import { HydratedDocument } from 'mongoose';
import { BaseEntity, BaseEntityField, IBaseEntity } from '../../shared/base';

@Schema()
@ObjectType()
export class Author extends BaseEntity implements IBaseEntity {
  @Prop(String)
  @Field(() => String, { nullable: true, description: 'Author name' })
  readonly name: string;

  constructor(base?: Partial<Author>) {
    super(base);
    Object.assign(this, base);
  }
}

export const AuthorSchema = SchemaFactory.createForClass(Author);
export type AuthorDocument = HydratedDocument<Author>;
AuthorSchema.index({ name: 'text' }, { weights: { name: 1 } });

export const AuthorFields = {
  ...BaseEntityField,
  name: 'name',
};
