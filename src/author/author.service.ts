import { Injectable, Logger } from '@nestjs/common';
import { InjectModel } from '@nestjs/mongoose';
import { Model, Types } from 'mongoose';
import { BaseServiceFactory } from '../shared/base';
import { DATABASE_CONNECTION, MODEL_NAME } from '../shared/config';
import {
  CreateAuthorInput,
  FilterByAuthorFields,
  GetAllAuthorInput,
  OrderByAuthorFields,
  PaginatedAuthorOutput,
  UpdateAuthorInput,
} from './dto';
import { Author } from './entities';

@Injectable()
export class AuthorService extends BaseServiceFactory<
  Author,
  CreateAuthorInput,
  UpdateAuthorInput,
  FilterByAuthorFields,
  OrderByAuthorFields,
  GetAllAuthorInput,
  PaginatedAuthorOutput
>(
  Author,
  CreateAuthorInput,
  UpdateAuthorInput,
  FilterByAuthorFields,
  OrderByAuthorFields,
  GetAllAuthorInput,
  PaginatedAuthorOutput,
) {
  constructor(
    @InjectModel(MODEL_NAME.AUTHOR, DATABASE_CONNECTION.MAIN_DB)
    private readonly AuthorModel: Model<Author>,
  ) {
    super(new Logger(AuthorService.name), AuthorModel, Author);
  }
  // get Id of all Authors
  async getIds(): Promise<Types.ObjectId[]> {
    const authors = await this.AuthorModel.find({}, { _id: 1 }).lean().exec();
    return authors.map((author) => author._id);
  }
}
