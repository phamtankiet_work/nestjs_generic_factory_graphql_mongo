import { ObjectType } from '@nestjs/graphql';
import { PaginatedInputFactory } from '../../shared/base';
import { Book } from '../entities';

@ObjectType()
export class PaginatedBookOutput extends PaginatedInputFactory<Book>(Book) {}
