import { InputType, registerEnumType } from '@nestjs/graphql';
import { getAllInputFactory } from '../../shared/base';
import { FilterByGeneric, OrderByGeneric } from '../../shared/graphql';
import { BookFields } from '../entities';

registerEnumType(BookFields, {
  name: 'BookFields',
  description: 'Book fields',
});

export const FilterByBookFields = FilterByGeneric(BookFields, 'BookFields');

export const OrderByBookFields = OrderByGeneric(BookFields, 'BookFields');
export type FilterByBookFields = InstanceType<typeof FilterByBookFields>;
export type OrderByBookFields = InstanceType<typeof OrderByBookFields>;

@InputType()
export class GetAllBookInput extends getAllInputFactory<
  FilterByBookFields,
  OrderByBookFields
>(FilterByBookFields, OrderByBookFields) {}
