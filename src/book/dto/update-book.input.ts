import { InputType, OmitType } from '@nestjs/graphql';
import { IUpdateInput } from '../../shared/base';
import { Book } from '../entities';

@InputType()
export class UpdateBookInput
  extends OmitType(Book, ['createdAt', 'updatedAt'] as const, InputType)
  implements IUpdateInput {}
