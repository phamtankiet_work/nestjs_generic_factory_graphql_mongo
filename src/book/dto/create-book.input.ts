import { InputType, OmitType } from '@nestjs/graphql';
import { ICreateInput } from '../../shared/base';
import { Book } from '../entities';

@InputType()
export class CreateBookInput
  extends OmitType(Book, ['_id', 'createdAt', 'updatedAt'] as const, InputType)
  implements ICreateInput {}
