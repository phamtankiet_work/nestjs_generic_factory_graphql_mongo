import { Module } from '@nestjs/common';
import { BookService } from './book.service';
import { BookResolver } from './book.resolver';
import { MongooseModule } from '@nestjs/mongoose';
import { MODEL_NAME, DATABASE_CONNECTION } from '../shared';
import { SharedModule } from '../shared/shared.module';
import { BookSchema } from './entities';
import { AuthorBookResolver } from './resolvers/author-book.resolver';

@Module({
  imports: [
    SharedModule,
    MongooseModule.forFeature(
      [
        {
          name: MODEL_NAME.BOOK,
          schema: BookSchema,
          collection: MODEL_NAME.BOOK,
        },
      ],
      DATABASE_CONNECTION.MAIN_DB,
    ),
  ],
  providers: [BookResolver, BookService, AuthorBookResolver],
})
export class BookModule {}
