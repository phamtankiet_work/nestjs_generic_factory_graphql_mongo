import { Logger } from '@nestjs/common';
import { Parent, ResolveField, Resolver } from '@nestjs/graphql';
import { Book } from '../entities';
import { BookService } from '../book.service';
import { Author } from '../../author/entities';
import { FlattenMaps } from 'mongoose';

@Resolver(() => Author)
export class AuthorBookResolver {
  private readonly logger: Logger = new Logger(AuthorBookResolver.name);
  constructor(private readonly BookService: BookService) {}
  // ====== Queries =======

  // ====== Mutations =======

  // ====== Resolvers =======
  // resolver Author group
  @ResolveField('Books', () => [Book])
  async getAllBooksByAuthor(
    @Parent() Author: Author,
  ): Promise<FlattenMaps<Book[]>> {
    return await this.BookService.getAllByAuthorId(Author._id);
  }
}
