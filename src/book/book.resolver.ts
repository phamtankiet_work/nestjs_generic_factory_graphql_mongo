import { Logger } from '@nestjs/common';
import { Resolver } from '@nestjs/graphql';
import { BaseResolverFactory } from '../shared/base';
import { BookService } from './book.service';
import {
  CreateBookInput,
  FilterByBookFields,
  GetAllBookInput,
  OrderByBookFields,
  PaginatedBookOutput,
  UpdateBookInput,
} from './dto';
import { Book } from './entities';

@Resolver(() => Book)
export class BookResolver extends BaseResolverFactory<
  Book,
  CreateBookInput,
  UpdateBookInput,
  FilterByBookFields,
  OrderByBookFields,
  GetAllBookInput,
  PaginatedBookOutput,
  BookService
>(
  Book,
  CreateBookInput,
  UpdateBookInput,
  GetAllBookInput,
  PaginatedBookOutput,
) {
  private readonly logger: Logger = new Logger(BookResolver.name);
  constructor(private readonly BookService: BookService) {
    super(BookService);
  }
  // ====== Queries =======

  // ====== Mutations =======

  // ====== Resolvers =======
}
