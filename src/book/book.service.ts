import { Injectable, Logger } from '@nestjs/common';
import { InjectModel } from '@nestjs/mongoose';
import { FlattenMaps, Model, Types } from 'mongoose';
import { BaseServiceFactory } from '../shared/base';
import { DATABASE_CONNECTION, MODEL_NAME } from '../shared/config';
import {
  CreateBookInput,
  FilterByBookFields,
  GetAllBookInput,
  OrderByBookFields,
  PaginatedBookOutput,
  UpdateBookInput,
} from './dto';
import { Book } from './entities';
import { OrderDirection } from '../shared/graphql/enums';

@Injectable()
export class BookService extends BaseServiceFactory<
  Book,
  CreateBookInput,
  UpdateBookInput,
  FilterByBookFields,
  OrderByBookFields,
  GetAllBookInput,
  PaginatedBookOutput
>(
  Book,
  CreateBookInput,
  UpdateBookInput,
  FilterByBookFields,
  OrderByBookFields,
  GetAllBookInput,
  PaginatedBookOutput,
) {
  private readonly logger: Logger = new Logger(BookService.name);
  constructor(
    @InjectModel(MODEL_NAME.BOOK, DATABASE_CONNECTION.MAIN_DB)
    private readonly BookModel: Model<Book>,
  ) {
    super(new Logger(BookService.name), BookModel, Book);
  }
  // get Id of all Books
  async getIds(): Promise<Types.ObjectId[]> {
    const books = await this.BookModel.find({}, { _id: 1 }).lean().exec();
    return books.map((book) => book._id);
  }

  // write function get all by authorId return promise leandocument array
  async getAllByAuthorId(
    authorId: Types.ObjectId,
  ): Promise<FlattenMaps<Book>[]> {
    try {
      // make sure author exists
      return new Promise<FlattenMaps<Book>[]>((resolve, reject) => {
        this.BookModel.find({ authorId: authorId })
          .sort({ createdAt: OrderDirection.ASC })
          .lean()
          .then((authorGroups) => resolve(authorGroups))
          .catch((err) => reject(err));
      });
    } catch (error) {
      this.logger.error(error);
      throw error;
    }
  }
}
