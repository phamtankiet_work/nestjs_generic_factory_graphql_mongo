import { Field, ID, ObjectType } from '@nestjs/graphql';
import { Prop, Schema, SchemaFactory } from '@nestjs/mongoose';
import mongoose, { HydratedDocument, Types } from 'mongoose';
import { BaseEntity, BaseEntityField, IBaseEntity } from '../../shared/base';
import { MODEL_NAME } from '../../shared';

@Schema()
@ObjectType()
export class Book extends BaseEntity implements IBaseEntity {
  @Prop(String)
  @Field(() => String, { nullable: true, description: 'Book name' })
  readonly name: string;

  @Prop({ type: mongoose.Schema.Types.ObjectId, ref: MODEL_NAME.AUTHOR })
  @Field(() => ID, { description: 'Author ID' })
  readonly authorId: Types.ObjectId;

  constructor(base?: Partial<Book>) {
    super(base);
    Object.assign(this, base);
  }
}

export const BookSchema = SchemaFactory.createForClass(Book);
export type BookDocument = HydratedDocument<Book>;
BookSchema.index({ name: 'text' }, { weights: { name: 1 } });

export const BookFields = {
  ...BaseEntityField,
  name: 'name',
  authorId: 'authorId',
};
