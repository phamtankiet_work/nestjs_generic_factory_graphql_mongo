import { Injectable } from '@nestjs/common';
import { config } from './shared';

@Injectable()
export class AppService {
  getHello(): string {
    return 'Hello World! version: ' + config.app.version;
  }
}
